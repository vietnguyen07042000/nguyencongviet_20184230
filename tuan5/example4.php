<?php
class Foo{
    static $vals;
    public static function __callStatic($name, $arguments)
    {
        if (!empty($arguments)){
            self::$vals[$name] = $arguments[0];
        }else{
            return self::$vals[$name];
        }
    }
}
Foo::username('john');
print Foo::username();
